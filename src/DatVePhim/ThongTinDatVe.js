import React, { Component } from 'react'
import {connect} from 'react-redux'
class ThongTinDatVe extends Component {
  // các phương thức trong component
  // phương thức này dùng để render giỏ hàng
  renderGioHangVe = () => {
    return this.props.gioHangVe.map((ve, index) => {
      return <tr key={index}>
        <th>{ve.soGhe}</th>
        <th>{ve.gia.toLocaleString()}</th>
        <th><div className='text-danger'>X</div></th>
      </tr>
    })
  }
  render() {
    let tongTienVe = this.props.gioHangVe.reduce((tst, ve, index) => {
      return tst += ve.gia
    }, 0)
    return (
      <div>
        <h1 style={{ textTransform: 'uppercase', fontSize: 30 }} className='mt-4'>Danh Sách Ghế Bạn Chọn</h1>
        <div className='text-left mt-3' style={{ fontSize: 20 }}>
          <div className='mb-2 d-flex align-items-center'>
            <button className='gheDuocChon mr-2'></button>
            Ghế đã đặt
          </div>
          <div className='mb-2 d-flex align-items-center'>
            <button className='gheDangChon mr-2'></button>
            Ghế đang đặt
          </div>
          <div className='mb-2 d-flex align-items-center'>
            <button className='ghe mr-2' style={{ marginLeft: 0 }}></button>
            Ghế chưa đặt
          </div>
        </div>
        <table className='table text-white mt-3' border='1'>
          <thead>
            <tr>
              <th>Số ghế</th>
              <th>Giá</th>
              <th>Hủy</th>
            </tr>
          </thead>
          <tbody>
          {this.renderGioHangVe()}
            <tr>
              <td>Tổng tiền</td>
              <td>{tongTienVe.toLocaleString()}</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

const mapStatetoProps = (state) => {
  return {
    gioHangVe: state.DatVePhimReducer.gioHangVe
  }
}

export default connect(mapStatetoProps)(ThongTinDatVe)