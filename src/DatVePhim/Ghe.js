import React, { Component } from 'react'
import {connect} from 'react-redux'
class Ghe extends Component {
    renderGhe = () => {
        return this.props.hangGhe.danhSachGhe.map((ghe, index) => {
            let cssGheDaDat = ''
            let disable = false
            if(ghe.daDat === true) {
                cssGheDaDat = 'gheDuocChon'
                disable = true
            }
            let cssGheDangDat = ''
            let indexGhedangDat = this.props.gioHangVe.findIndex(veDangDat => {
                return veDangDat.soGhe === ghe.soGhe
            })
            if(indexGhedangDat !== -1) {
                cssGheDangDat = 'gheDangChon'
            }
            if(this.props.hangGhe.hang === '') {
                return <button className='rowNumber mb-4' key={index}>{ghe.soGhe}</button>
            }else {
                return <button disabled={disable} onClick={() => this.props.datVePhim(ghe)} className={`ghe ${cssGheDaDat} ${cssGheDangDat}`} key={index}>{ghe.soGhe}</button>
            }
        })
    }
  render() {
    return (
      <div>
        <span style={{fontSize: 20, fontWeight: 'bold', width: 10, textAlign: 'right'}}>{this.props.hangGhe.hang}</span> {this.renderGhe()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
       gioHangVe:  state.DatVePhimReducer.gioHangVe
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        datVePhim: (ve) => {
            let action = {
                type: 'DAT_VE_PHIM',
                ve
            }
            dispatch(action)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Ghe)