import React, { Component } from 'react'
import Ghe from './Ghe'
import {connect} from 'react-redux'

class DanhSachGhe extends Component {
    renderHangGhe = () => {
        return this.props.chairList.map((hangGhe, index) => {
            return <Ghe key={index} hangGhe={hangGhe}/>
        })
    }
    render() {
    return (
      <div className='mt-4'>
        {this.renderHangGhe()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    // nó trả cho chúng ta state chính là rootReducer
    return {
        chairList: state.DatVePhimReducer.chairList
    }
}

export default connect(mapStateToProps)(DanhSachGhe)