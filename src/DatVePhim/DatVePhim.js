import React, { Component } from 'react'
import DanhSachGhe from './DanhSachGhe'
import './DatVePhim.css'
import ThongTinDatVe from './ThongTinDatVe'

export default class DatVePhim extends Component {
    render() {
        return (
            <div className='bookingMovie'>
                <div className='overlay'></div>
                <div className='container-fluid text-center'>
                    <div className="row">
                        <div className="col-8">
                            <h1 className='text-uppercase' style={{fontWeight: 500}} >Đặt vé xem phim CyberLearn.vn</h1>
                            <h3>Màn hình</h3>
                            <div className='screen'></div>
                            <DanhSachGhe/>
                        </div>
                        <div className='col-4'>
                            <ThongTinDatVe/>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}
