import chairData from '../../data/danhSachGhe.json'
const stateDefault = {
    chairList: chairData,
    gioHangVe: [] 
}

const DatVePhimReducer = (state = stateDefault, action) => {
    switch(action.type) {
        case 'DAT_VE_PHIM': {
            let cloneGioHangVe = [...state.gioHangVe]
            let veMoi = {...action.ve}
            let indexGiohang = state.gioHangVe.findIndex(ve => {
                return ve.soGhe === action.ve.soGhe
            })
            if(indexGiohang !== -1) {
                cloneGioHangVe.splice(indexGiohang, 1)
            }else {
                cloneGioHangVe.push(veMoi)
            }
            state.gioHangVe = cloneGioHangVe
            return {...state}
        }
        default: {
            return state
        }
    }
}

export default DatVePhimReducer